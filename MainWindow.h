#pragma once


#include <qopenglwidget.h>
#include <qevent.h>
#include <qpoint.h>
#include <qpushbutton.h>
#include <qgroupbox.h>
#include <qlistview.h>
#include <qstringlistmodel.h>
#include <qspinbox.h>
#include <qlabel.h>
#include <qjsondocument.h>

#include <qopenglbuffer.h>
#include <qopenglvertexarrayobject.h>
#include <qopenglfunctions.h>
#include <qopenglshaderprogram.h>

#include "body.cuh"
#include "Logger.h"


class MainWindow 
	: public QOpenGLWidget, protected QOpenGLFunctions {
	Q_OBJECT

public:
	MainWindow(QWidget *parent = Q_NULLPTR);
	~MainWindow();

private:
	// variables
	bool run;
	int xMargin, yMargin;
	int logHeight, menuWidth;
	int N, defaultN;
	double scale;
	double timestep;
	int followedBodyId;
	Logger* logger;
	cudaGraphicsResource* resource;
	Coordinates<>* followedBody;
	GLdouble softSquare;


	// Qt variables
	QLabel* timestepLabel, * followLabel, *inputLabel;
	QPushButton* menuButton;
	QGroupBox* menuGroup;
	QDoubleSpinBox* timestepSpinBox;
	QListView* namesList, *inputList;
	QStringListModel* namesModel, *inputModel;
	QPoint previousMousePosition;
	QPushButton* randomInputButton;
	QPushButton* openFileButton;
	QJsonDocument doc;
	

	// openGL variables
	QOpenGLBuffer vertexBuffer;
	QOpenGLBuffer colorBuffer;
	QOpenGLVertexArrayObject vao;
	QOpenGLShaderProgram shaderProgram;


	// methods
	void setupUi();
	void setupMenu();
	void setupLog();
	void getModel(QString);
	void loadFile();
	void simulationLoop();
	void initializeBuffers();
	void loadDataBuffer(QJsonArray);
	void loadColorBuffer(QJsonArray);
	void generateRandomInput(bool);


	// Qt methods
	void wheelEvent(QWheelEvent*);
	void showEvent(QShowEvent*);
	void closeEvent(QCloseEvent*);


	// openGL methods
	void initializeGL();
	void paintGL();
	void resizeGL(int, int);


public slots:
	void slot_start();
	void slot_hideMenu();
	void slot_follow(QModelIndex);
	void slot_input(QModelIndex);
	void slot_timestampChanged(double);
};