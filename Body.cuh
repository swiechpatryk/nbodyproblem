#pragma once


#include <cuda_runtime.h>

#include <qstring.h>

#include "Point.cuh"
#include "Vector.cuh"

#define __hostdev__ __host__ __device__

template <typename T = GLdouble>
struct Body {
	Point<T> p;
	Vector<T> v;
	T mass;

	__hostdev__ ~Body() {}
	__hostdev__ Body() {
		p = Point<T>(0, 0, 0);
		v = Vector<T>(0, 0, 0);
		mass = 0;
	}

	__hostdev__ Body(T x, T y, T z, T v0, T v1, T v2, T m) {
		p = Point<T>(x, y, z);
		v = Vector<T>(v0, v1, v2);
		mass = m;
	}

	__hostdev__ void leapFrog(Vector<T> a, double dt) {
		v += a * dt;
		p += v * dt;
	}

	__host__ QString toString() {
		QString str = p.toString() + "   " + v.toString() + "   mass = " + QString::number(mass);
		return str;
	}
};