#include "Logger.h"
#include <qdatetime.h>

#if (defined(__LOG) || defined(__DEBUG)) && (defined(__DEBUG_FILE) || defined(__LOG_FILE))
Logger::Logger(QWidget *parent) : QTextEdit(parent){
	logFile.setFileName(QString("Log.txt"));
	writeToFile("Start: " + QDateTime::currentDateTime().toString("d-M-yyyy H:mm:ss"));
}
#elif (defined(__LOG) || defined(__DEBUG))
Logger::Logger(QWidget* parent) : QTextEdit(parent) {

}
#elif defined(__DEBUG_FILE) || defined(__LOG_FILE)
Logger::Logger(){
	logFile.setFileName(QString("Log.txt"));
	writeToFile("Start: " + QDateTime::currentDateTime().toString("d-M-yyyy H:mm:ss"));
}
#endif


Logger::~Logger(){
	writeToFile("End: " + QDateTime::currentDateTime().toString("d-M-yyyy H:mm:ss") + "\n\n");
	writeToFile("---------------------------------------------------------------------------------\n\n");
}

void Logger::debug(QString text, bool isError){
#ifdef __DEBUG_FILE
	if((__DEBUG_ONLY_ERROR && isError) || !__DEBUG_ONLY_ERROR)
		writeToFile(text);
#endif

#ifdef __DEBUG
	if ((__DEBUG_ONLY_ERROR && isError) || !__DEBUG_ONLY_ERROR)
		writeOnTextEdit(text);
#endif
}

void Logger::log(QString text) {
#ifdef __LOG_FILE
	writeToFile(text);
#endif

#ifdef __LOG
	writeOnTextEdit(text);
#endif
}

#if defined(__LOG) || defined(__DEBUG)
void Logger::writeOnTextEdit(QString text) {
	this->append(text);
}
#endif

#if defined(__DEBUG_FILE) || defined(__LOG_FILE)
void Logger::writeToFile(QString text) {
	logFile.setFileName(QString("Log.txt"));
	logFile.open(QIODevice::WriteOnly | QIODevice::Append);

	QTextStream stream(&logFile);
	stream << text + "\n";
	logFile.close();
}
#endif

int Logger::logHeight() {
#if defined(__LOG) || defined(__DEBUG)
	return 150;
#else
	return 0;
#endif
}