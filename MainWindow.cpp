#include "MainWindow.h"

#include <qstring.h>
#include <qcoreapplication.h>
#include <qthread.h>
#include <qtimer.h>
#include <qfile.h>
#include <qjsonarray.h>
#include <qjsonobject.h>
#include <qrandom.h>

#include "kernel.cuh"


MainWindow::MainWindow(QWidget *parent) 
	: QOpenGLWidget(parent) {

	setWindowTitle("N-body simulation");
	setMinimumHeight(400);
	setMinimumWidth(400);
	resize(1000, 800);

	run = false;
	xMargin = 25;
	yMargin = 25;
	followedBodyId = -1;
	followedBody = new Coordinates<>();

	timestep = 1;
	scale = 1;

	softSquare = 0.000125;

	N = 0;
	defaultN = 1500;

	setupUi();
}

MainWindow::~MainWindow() {
	run = false;

	delete menuButton;
	delete namesList;
	delete menuGroup;
	delete logger;
	vertexBuffer.destroy();
	vao.destroy();
}

void MainWindow::wheelEvent(QWheelEvent* e) {
	int delta = e->delta();

	if (e->orientation() == Qt::Vertical) {
		if (delta > 0) {
			scale *= 1.15f;
		}
		else if (delta < 0) {
			scale *= 0.85f;
		}
	}
	e->accept();
}

void MainWindow::showEvent(QShowEvent* e) {
	QOpenGLWidget::showEvent(e);
	QTimer::singleShot(50, this, SLOT(slot_start()));
	e->accept();
}

void MainWindow::closeEvent(QCloseEvent* e) {
	run = false;
	e->accept();
}

//---------------------------------------------------------------------OpenGL---------------------------------------------------------------

void MainWindow::initializeGL() {
	initializeOpenGLFunctions();
	glClearColor(0, 0, 0, 1);


	glEnable(GL_PROGRAM_POINT_SIZE);
	glEnable(GL_POINT_SMOOTH);
	glPointSize(4);

	if(shaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex, ":/vertexShader.vsh"))
		logger->debug("ShaderProgram: Vertex shader added successfully", 1);
	else
		logger->debug("ShaderProgram: Error while adding vertex shader", 1);
	if(shaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/fragmentShader.fsh"))
		logger->debug("ShaderProgram: Fragment shader added successfully", 1);
	else
		logger->debug("ShaderProgram: Error while adding vertex shader", 1);
	if(shaderProgram.link())
		logger->debug("ShaderProgram: Link successfull", 1);
	else
		logger->debug("ShaderProgram: Error while linking", 1);
	shaderProgram.bind();


	if (vao.create())
		logger->debug("Vao created successfully", 1);
	else
		logger->debug("Error on Vao creation", 1);


	if (vertexBuffer.create())
		logger->debug("Buffer created successfully", 1);
	else
		logger->debug("Error on buffer creation", 1);


	if (colorBuffer.create())
		logger->debug("Color buffer created successfully", 1);
	else
		logger->debug("Error on color buffer creation", 1);

	vao.bind();
	initializeBuffers();
	vao.release();
	shaderProgram.release();

	vertexBuffer.bind();
	cudaError e = cudaGraphicsGLRegisterBuffer(&resource, vertexBuffer.bufferId(), cudaGraphicsRegisterFlagsNone);
	logger->debug("Register buffer " + QString(cudaGetErrorString(e)), e);
	vertexBuffer.release();
}

void MainWindow::paintGL() {
	glClear(GL_COLOR_BUFFER_BIT);

	QMatrix4x4 mMatrix;
	mMatrix.setToIdentity();
	mMatrix.scale(scale);

	shaderProgram.bind();

	if (followedBodyId == 0) followedBody->set(0, 0, 0);
	if (followedBodyId > 0) {
		vertexBuffer.bind();
		vertexBuffer.read((followedBodyId - 1) * sizeof(Body<>), followedBody, sizeof(Coordinates<>));
		vertexBuffer.release();
	}
	mMatrix.translate(-followedBody->x, -followedBody->y, -followedBody->z);

	shaderProgram.setUniformValue("mvpMatrix", mMatrix);

	vao.bind();
	glDrawArrays(GL_POINTS, 0, N);
	vao.release();

	shaderProgram.release();
}

void MainWindow::resizeGL(int width, int height) {
	menuButton->setGeometry(QRect(width - 80 - xMargin, yMargin, 80, 24));
	menuGroup->setGeometry(QRect(width - xMargin - menuWidth, 2 * yMargin + menuButton->height(), menuWidth, height - 3 * yMargin - menuButton->height()));
#if defined(__LOG) || defined(_DEBUG_)

	logger->setGeometry(QRect(xMargin, height - logHeight - yMargin, width - 3 * xMargin - menuGroup->width(), logHeight));

#endif
}

//---------------------------------------------------------------------Ui-------------------------------------------------------------------

void MainWindow::setupUi() {
	menuButton = new QPushButton(this);
	menuButton->setText("Menu");
	menuButton->setGeometry(QRect(width() - 80 - xMargin, yMargin, 80, 24));
	menuButton->setCheckable(true);
	menuButton->show();

	connect(menuButton, SIGNAL(clicked()), SLOT(slot_hideMenu()));

	setupMenu();
	setupLog();
}

void MainWindow::setupMenu() {
	menuWidth = 170;
	menuGroup = new QGroupBox("", this);
	menuGroup->setVisible(false);
	menuGroup->setGeometry(QRect(width() - xMargin - menuWidth, 2 * yMargin + menuButton->height(), menuWidth, height() - 3 * yMargin - menuButton->height()));
	menuGroup->setStyleSheet(" QGroupBox {border : 1px solid black } ");

	timestepLabel = new QLabel(menuGroup);
	timestepLabel->setText("Timestamp");
	timestepLabel->setGeometry(QRect(5, 5, menuGroup->width() - 10, 13));
	timestepLabel->setStyleSheet("QLabel { color : white; }");

	timestepSpinBox = new QDoubleSpinBox(menuGroup);
	timestepSpinBox->setDecimals(6);
	timestepSpinBox->setRange(0.000001, 10);
	timestepSpinBox->setGeometry(QRect(5, 18, menuGroup->width() - 10, 20));
	timestepSpinBox->setStepType(QAbstractSpinBox::AdaptiveDecimalStepType);

	inputLabel = new QLabel(menuGroup);
	inputLabel->setText("Input");
	inputLabel->setGeometry(QRect(5, 43, menuGroup->width() - 10, 13));
	inputLabel->setStyleSheet("QLabel { color : white; }");

	inputList = new QListView(menuGroup);
	inputList->setGeometry(QRect(5, 61, menuGroup->width() - 10, 100));
	inputModel = new QStringListModel();
	inputList->setModel(inputModel);
	inputList->setEditTriggers(QAbstractItemView::NoEditTriggers);

	followLabel = new QLabel(menuGroup);
	followLabel->setText("Follow");
	followLabel->setGeometry(QRect(5, 166, menuGroup->width() - 10, 13));
	followLabel->setStyleSheet("QLabel { color : white; }");
	followLabel->setVisible(false);

	namesList = new QListView(menuGroup);
	namesList->setGeometry(QRect(5, 184, menuGroup->width() - 10, 100));
	namesModel = new QStringListModel();
	namesList->setModel(namesModel);
	namesList->setVisible(false);
	namesList->setEditTriggers(QAbstractItemView::NoEditTriggers);

	connect(timestepSpinBox, SIGNAL(valueChanged(const double)), SLOT(slot_timestampChanged(double)));
	connect(namesList, SIGNAL(clicked(const QModelIndex)), SLOT(slot_follow(QModelIndex)));
	connect(inputList, SIGNAL(clicked(const QModelIndex)), SLOT(slot_input(QModelIndex)));
}

void MainWindow::setupLog() {
	logHeight = Logger::logHeight();
#if defined (__LOG) || defined(__DEBUG)
	logger = new Logger(this);
	logger->setGeometry(QRect(xMargin, height() - logHeight - yMargin, width() - 3 * xMargin - menuGroup->width(), logHeight));
	logger->setVisible(false);
	logger->setReadOnly(true);
#else
	logger = new Logger();
#endif

	setLogger(logger);
}

//---------------------------------------------------------------------Input-------------------------------------------------------------------

void MainWindow::initializeBuffers() {
	N = defaultN;

	vertexBuffer.bind();
	vertexBuffer.allocate(N * sizeof(Body<>));
	shaderProgram.enableAttributeArray("vertex");
	shaderProgram.setAttributeBuffer("vertex", GL_DOUBLE, 0, 3, sizeof(Body<>));
	vertexBuffer.release();
	colorBuffer.bind();
	colorBuffer.allocate(N * 3 * sizeof(GLdouble));
	shaderProgram.enableAttributeArray("color");
	shaderProgram.setAttributeBuffer("color", GL_DOUBLE, 0, 3, 0);
	colorBuffer.release();

	loadFile();

	
	scale = doc.object().value("Random").toObject().value("Default scale").toDouble();
	timestep = doc.object().value("Random").toObject().value("Default timestep").toDouble();
	timestepSpinBox->setValue(timestep);
	generateRandomInput(0);
	followLabel->setVisible(false);
	namesList->setVisible(false);
}

void MainWindow::getModel(QString name) {
	QJsonObject obj = doc.object().value(name).toObject();
	QJsonArray arr = obj.value("Bodies").toArray();
	scale = obj.value("Default scale").toDouble();
	timestep = obj.value("Default timestep").toDouble();
	timestepSpinBox->setValue(timestep);
	N = arr.size();

	loadDataBuffer(arr);
	loadColorBuffer(arr);

	followLabel->setVisible(true);
	namesList->setVisible(true);
}

void MainWindow::loadFile() {
	QString fileName = "input.json";

	QFile file(fileName);
	logger->debug("Loading data from file: " + fileName, 1);

	if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
		QString s = file.readAll();
		file.close();

		QStringList names;
		doc = QJsonDocument::fromJson(s.toUtf8());
		foreach(QString s, doc.object().keys()) {
			names << s;
		}
		inputModel->setStringList(names);

		logger->debug("End of loading data", 1);
	}
	else logger->debug("Error on file open", 1);
}

void MainWindow::loadDataBuffer(QJsonArray arr) {
	if (vertexBuffer.bind())
		logger->debug("Buffer bind successfull", 0);
	else
		logger->debug("Error on bind buffer", 1);

	QStringList names;
	names << "" << "Center";
	for (int i = 0; i < N; i++) {
		Body<>* body = new Body<>(arr.at(i)["Position"]["x"].toDouble(), arr.at(i)["Position"]["y"].toDouble(), arr.at(i)["Position"]["z"].toDouble(),
			arr.at(i)["Velocity"]["x"].toDouble(), arr.at(i)["Velocity"]["y"].toDouble(), arr.at(i)["Velocity"]["z"].toDouble(), arr.at(i)["Mass"].toDouble());
		vertexBuffer.write(i * sizeof(Body<>), body, sizeof(Body<>));
		logger->log(arr.at(i)["Name"].toString() + ":\t" + body->toString());
		names << arr.at(i)["Name"].toString();

		delete body;
	}
	namesModel->setStringList(names);
	logger->log(QString::number(arr.size()) + " bodies loaded");

	vertexBuffer.release();
}

void MainWindow::loadColorBuffer(QJsonArray arr) {
	if (colorBuffer.bind())
		logger->debug("Color buffer bind successfull", 0);
	else
		logger->debug("Error on bind color buffer", 1);

	GLdouble* inp = new GLdouble[3];
	for (int i = 0; i < N; i++) {
		inp[0] = arr.at(i)["Color"]["Red"].toDouble();
		inp[1] = arr.at(i)["Color"]["Green"].toDouble();
		inp[2] = arr.at(i)["Color"]["Blue"].toDouble();
		colorBuffer.write(i * 3 * sizeof(GLdouble), inp, 3 * sizeof(GLdouble));
	}
	delete[] inp;

	colorBuffer.release();
}

void MainWindow::generateRandomInput(bool star) {
	N = defaultN;

	if (vertexBuffer.bind())
		logger->debug("Buffer bind successfull", 0);
	else
		logger->debug("Error on bind buffer", 1);

	for (int i = 0; i < N; i++) {
		Body<>* body = new Body<>(QRandomGenerator::global()->bounded(2.0) - 1, QRandomGenerator::global()->bounded(2.0) - 1, QRandomGenerator::global()->bounded(2.0) - 1, 
			QRandomGenerator::global()->bounded(2.0) - 1, QRandomGenerator::global()->bounded(2.0) - 1, QRandomGenerator::global()->bounded(2.0) - 1, 1);
		vertexBuffer.write(i * sizeof(Body<>), body, sizeof(Body<>));

		delete body;
	}
	if(star) vertexBuffer.write(0, new Body<>(QRandomGenerator::global()->bounded(4.0) - 2, QRandomGenerator::global()->bounded(4.0) - 2, 
		QRandomGenerator::global()->bounded(4.0) - 2, 0, 0, 0, 1000000), sizeof(Body<>));

	vertexBuffer.release();

	if (colorBuffer.bind())
		logger->debug("Color buffer bind successfull", 0);
	else
		logger->debug("Error on bind color buffer", 1);

	GLdouble* inp = new GLdouble[3];
	for (int i = 0; i < N; i++) {
		inp[0] = 1;
		inp[1] = 0.82;
		inp[2] = 0.25;
		colorBuffer.write(i * 3 * sizeof(GLdouble), inp, 3 * sizeof(GLdouble));
	}
	if (star) {
		inp[0] = 1;
		inp[1] = 0;
		inp[2] = 0;
		colorBuffer.write(0, inp, 3 * sizeof(GLdouble));
	}
	delete[] inp;

	colorBuffer.release();
}

void MainWindow::simulationLoop() {
	int threadsPerBlock = ((int)((N - 1) / 32) + 1) * 32;
	if (threadsPerBlock > 1024) threadsPerBlock = 1024;
	int numblocks = N;

	int sleepTime = 100 / 60;
	run = true;
	logger->debug("Start simulation", 1);
	while (run) {
		cudaEvent_t finished;
		cudaEventCreate(&finished);
		runKernel(vertexBuffer, N, numblocks, threadsPerBlock, resource, timestep, softSquare);
		cudaEventRecord(finished, 0);

		while (cudaEventQuery(finished) == cudaErrorNotReady) {
			QCoreApplication::processEvents(QEventLoop::AllEvents, sleepTime);
			QThread::msleep(sleepTime);
		}
		update();
	}
	logger->debug("Stop simulation", 1);
}

//---------------------------------------------------------------------Slots-------------------------------------------------------------------

void MainWindow::slot_start() {
	simulationLoop();
}

void MainWindow::slot_hideMenu() {
	if (menuButton->isChecked()) {
		if(width() > menuWidth + 2 * xMargin)
			menuGroup->setVisible(true);
#ifdef __LOG
		if(height() > logHeight + 2 * yMargin)
			logger->setVisible(true);
#endif
	}
	else {
		menuGroup->setVisible(false);
#ifdef __LOG
		logger->setVisible(false);
#endif
	}
}

void MainWindow::slot_follow(QModelIndex index) {
	followedBodyId = index.row() - 1;
}

void MainWindow::slot_input(QModelIndex index) {
	if (index.data().toString() == QString("Random")) {
		scale = doc.object().value("Random").toObject().value("Default scale").toDouble();
		timestep = doc.object().value("Random").toObject().value("Default timestep").toDouble();
		timestepSpinBox->setValue(timestep);
		generateRandomInput(0);
		followLabel->setVisible(false);
		namesList->setVisible(false);
	}
	else if (index.data().toString() == QString("Random (Star)")) {
		scale = doc.object().value("Random (Star)").toObject().value("Default scale").toDouble();
		timestep = doc.object().value("Random (Star)").toObject().value("Default timestep").toDouble();
		timestepSpinBox->setValue(timestep);
		generateRandomInput(1);
		followLabel->setVisible(false);
		namesList->setVisible(false);
	}
	else {
		getModel(index.data().toString());
	}
}

void MainWindow::slot_timestampChanged(double t) {
	timestep = t;
}