#pragma once


#include <qstring.h>
#include "Coordinates.cuh"

template <typename T>
class Point3d;

template <typename T = GLdouble>
class Vector : public Coordinates<T> {
public:
	__hostdev__ Vector() : Coordinates() {}
	__hostdev__ Vector(T xx, T yy, T zz) : Coordinates(xx, yy, zz) {}
	__hostdev__ ~Vector() {}

	__hostdev__ T len();

	__hostdev__ Vector operator +(const Vector& obj) {
		return Vector(x + obj.x, y + obj.y, z + obj.z);
	}
	
	__hostdev__ Vector operator +=(const Vector& obj) {
		x += obj.x;
		y += obj.y;
		z += obj.z;
		return *this;
	}
	
	__hostdev__ Vector operator *(const double& obj) {
		return Vector(x * obj, y * obj, z * obj);
	}
	
	__hostdev__ Vector operator /(const double& obj) {
		return Vector(x / obj, y / obj, z / obj);
	}
	
	__hostdev__ Vector operator -() {
		return Vector(-x, -y, -z);
	}
	
	__hostdev__ Vector& operator =(const Vector& obj) {
		x = obj.x;
		y = obj.y;
		z = obj.z;
		return *this;
	}

	QString toString();
};

template <typename T>
T Vector<T>::len() {
	return sqrtf(x * x + y * y + z * z);
}

template <typename T>
QString Vector<T>::toString() {
	return "V" + Coordinates::toString();
}