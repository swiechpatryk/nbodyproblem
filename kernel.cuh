#pragma once


#include <cuda.h>
#include <cuda_runtime_api.h>


#ifndef __CUDACC__
#define __CUDACC__
#endif
#include "device_launch_parameters.h"

#include <qopenglbuffer.h>
#include <qopengl.h>
#include <cuda_gl_interop.h>
#include "Logger.h"

void setLogger(Logger *l);
void runKernel(QOpenGLBuffer bufferObj, GLint size, int numblocks, int threadsPerBlock, cudaGraphicsResource* resource, GLdouble timestep, GLdouble softSquare);