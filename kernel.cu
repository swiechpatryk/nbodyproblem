#include "kernel.cuh"

#include "body.cuh"
#include "Logger.h"

#include <qstring.h>

#define SHAREDSIZE 1024

Logger* logger;

void setLogger(Logger* l) {
	logger = l;
}

__hostdev__ Vector<> bodyInteraction(Body<> b1, Body<> b2, GLdouble softSquare) {
	Vector<double> d = b1.p - b2.p;

	double dist = d.len();

	dist = dist * dist + softSquare;

	dist = (double)1 / sqrtf(dist);
	dist = dist * dist * dist;
	double s = dist * b1.mass;

	return d * s;
}

__global__ void kernel(Body<>* b, GLint size, GLdouble timestep, GLdouble softSquare) {
	__shared__ GLdouble a[3][SHAREDSIZE];
	int id;

	Vector<> f;
	for (int i = 0; i < size; i += blockDim.x) {
		id = threadIdx.x + i;
		if (id < size) {
			f += bodyInteraction(b[id], b[blockIdx.x], softSquare);
		}
	}
	a[0][threadIdx.x] = f.x;
	a[1][threadIdx.x] = f.y;
	a[2][threadIdx.x] = f.z;

	__syncthreads();

	id = SHAREDSIZE / 2;
	while (id > 0) {
		if (threadIdx.x < id) {
			a[0][threadIdx.x] += a[0][threadIdx.x + id];
			a[1][threadIdx.x] += a[1][threadIdx.x + id];
			a[2][threadIdx.x] += a[2][threadIdx.x + id];
		}
		id = id >> 1;
		__syncthreads();
	}

	if (threadIdx.x == 0) {
		b[blockIdx.x].leapFrog(Vector<>(a[0][0], a[1][0], a[2][0]), timestep);
	}
}

void runKernel(QOpenGLBuffer vertexBuffer, GLint size, int numblocks, int threadsPerBlock, cudaGraphicsResource* resource, GLdouble timestep, GLdouble softSquare) {
	Body<>* gpu_bodies;
	size_t bodies_size;

	cudaError e;

	vertexBuffer.bind();
	e = cudaGraphicsMapResources(1, &resource, NULL);
	logger->debug("Map resources " + QString(cudaGetErrorString(e)), e);
	vertexBuffer.release();

	e = cudaGraphicsResourceGetMappedPointer((void**)& gpu_bodies, &bodies_size, resource);
	logger->debug("Get mapped pointer " + QString(cudaGetErrorString(e)), e);

	kernel <<<numblocks, threadsPerBlock>>> (gpu_bodies, size, timestep, softSquare);
	e = cudaGraphicsUnmapResources(1, &resource, NULL);
	logger->debug("Unmap resources " + QString(cudaGetErrorString(e)), e);
}