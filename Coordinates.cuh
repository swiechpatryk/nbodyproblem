#pragma once


#include <cuda_runtime.h>

#include <qstring.h>

#define __hostdev__ __host__ __device__

template <typename T = GLdouble>
class Coordinates {
public:
	T x, y, z;

	__hostdev__ Coordinates();
	__hostdev__ Coordinates(T, T, T);
	__hostdev__ void set(T, T, T);

	QString toString();
};

template <typename T>
Coordinates<T>::Coordinates() {
	x = 0;
	y = 0;
	z = 0;
}

template <typename T>
Coordinates<T>::Coordinates(T xx, T yy, T zz) {
	x = xx;
	y = yy;
	z = zz;
}

template <typename T>
void Coordinates<T>::set(T xx, T yy, T zz) {
	x = xx;
	y = yy;
	z = zz;
}

template <typename T>
QString Coordinates<T>::toString() {
	return "(" + QString::number(x) + ", " + QString::number(y) + ", " + QString::number(z) + ")";
}