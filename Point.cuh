#pragma once


#include <qstring.h>
#include "Coordinates.cuh"
#include "Vector.cuh"

template <typename T>
class Point : public Coordinates<T>{
public:
	__hostdev__ Point() : Coordinates() {}
	__hostdev__ Point(T xx, T yy, T zz) : Coordinates(xx, yy, zz) {}
	__hostdev__ ~Point(){}

	__hostdev__ Point operator +(Vector<> const& obj) {
		return Point(x + obj.x, y + obj.y, z + obj.z);
	}
	
	__hostdev__ Vector<> operator -(Point const &obj) {
		return Vector<>(x - obj.x, y - obj.y, z - obj.z);
	}

	__hostdev__ Point operator +=(Vector<> const &obj) {
		x += obj.x;
		y += obj.y;
		z += obj.z;
		return *this;
	}

	QString toString();
};

template <typename T>
QString Point<T>::toString() {
	return "P" + Coordinates::toString();
}