


//#define __LOG
//#define __LOG_FILE
//#define __DEBUG
#define __DEBUG_FILE
#define __DEBUG_ONLY_ERROR 1


#include <qstring.h>

#if (defined(__LOG) || defined(__DEBUG))
	#include <qtextedit.h>
	#include <qwidget.h>
#endif

#if defined(__DEBUG_FILE) || defined(__LOG_FILE)
	#include <qfile.h>
	#include <qtextstream.h>
#endif // __DEBUG_FILE

#pragma once
class Logger 
#if defined(__LOG) || defined(__DEBUG)
	: public QTextEdit
#endif
{
private:
#if defined(__DEBUG_FILE) || defined(__LOG_FILE)
	QFile logFile;

	void writeToFile(QString);
#endif // __DEBUG_FILE


#if (defined(__LOG) || defined(__DEBUG))
	void writeOnTextEdit(QString);
#endif


public:
#if defined(__LOG) || defined(__DEBUG)
	Logger(QWidget *);
#elif defined(__DEBUG_FILE) || defined(__LOG_FILE)
	Logger();
#endif
	~Logger();

	void debug(QString, bool);
	void log(QString);
	static int logHeight();
};